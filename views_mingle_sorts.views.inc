<?php

/**
 * @file
 * Views integration for views_mingle_sorts.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_mingle_sorts_views_data_alter(&$data) {
  $data['views']['views_mingle_sorts'] = array(
    'title' => t('Mingle sorts'),
    'help' => t('Sort several fields as one.'),
    'sort' => array(
      'handler' => 'views_mingle_sorts_handler_sort',
    ),
  );
}
