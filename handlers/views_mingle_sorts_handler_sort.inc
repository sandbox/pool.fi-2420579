<?php

/**
 * @file
 * Definition of views_mingle_sorts_handler_sort.
 */

/**
 * Handle sorting by selected content type(s).
 */
class views_mingle_sorts_handler_sort extends views_handler_sort {

  /**
   * Node type options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $display = $this->view->display[$this->view->current_display];
    $handlers = $display->handler->handlers;
    if (isset($handlers['sort'])) {
      $options = array();
      foreach ($handlers['sort'] as $key => $handler) {
        if ($handler === $this) {
          // We will not be able to alter sorts with higher weight than
          // this field, as they will be added to the query after this
          // sort is added.
          break;
        }
        $options[$key] = $handler->ui_name();
      }
      // Add content type checkboxes.
      $form['sorts'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Select which sorts to combine'),
        '#options' => $options,
        '#default_value' => $this->options['sorts'],
      );
    }
  }

  /**
   * Default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['sorts'] = array('default' => array());
    return $options;
  }

  /**
   * Add sort to query
   */
  function query() {
    $this->ensure_my_table();

    $display = $this->view->display[$this->view->current_display];
    $sort_handlers = $display->handler->handlers['sort'];

    $field_aliases = array();
    $count = 0;
    foreach($this->options['sorts'] as $key) {
      if (isset($sort_handlers[$key])) {
        $handler = $sort_handlers[$key];
        $field_alias = $this->query->field_aliases[$handler->table][$handler->field];
        $field_aliases[$field_alias] = $handler->table . '.' . $handler->field;
        $count++;
      }
    }

    // Build conditional sort.
    if ($count) {
      $case_sort = '(CASE ';
      foreach ($this->query->orderby as $key => $orderby) {
        $field = $orderby['field'];
        if (isset($field_aliases[$field])) {
          $case_sort .= --$count ? 'WHEN ' . $field_aliases[$field] . ' THEN ' : 'ELSE ';
          $case_sort .=  $field_aliases[$field] . ' ';
          // Remove from array.
          unset($this->query->orderby[$key]);

          // Prevent duplicates.
          unset($field_aliases[$field]);
        }
      }
      $case_sort .= 'END)';
      $this->query->add_orderby(NULL, $case_sort, $this->options['order'], $this->options['id']);
    }
  }
}
